import React from "react";
import { Router, Route } from "dva/router";
import ExamplePage from "./pages/example";

function RouterConfig({ history }) {
  return (
    <Router history={history}>
      <Route path="/example" component={ExamplePage} />
    </Router>
  );
}

export default RouterConfig;
